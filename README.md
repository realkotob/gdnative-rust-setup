# Usage

1. backup your entire project, if you’re running the GDNative + Rust setup
2. on your existing project.
1. create a Godot project
1. goto the project folder
1. paste setup.sh
1. run ```$ sh setup.sh``` in terminal
1. in Godot editor create a Node. save it Main.tscn
1. assign gdnative script to the Main.tscn
    1. language: NativeScript
    1. Inherits: Node
    1. Class Name: HelloWorld
    1. Path: Main.gdns
1. load the "game_gdnativelibrary.gdnlib" to the created script's Library property
1. Command + B to build the project.
1. in the godot console must see the "hello, world..." message.